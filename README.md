This application is manages two types of users (Employee and Admin).
An admin user will be added to the database when the application starts with credentials (admin/adminadmin)
along with some predefined weather notes for the user to see if the admin didn't enter today's weather note.

the webservice used to get weather info is yahoo weather service, below is a sample Json object response:
{"query":{"count":1,"created":"2017-05-19T20:19:06Z","lang":"en-US","results":{"channel":{"item":{"condition":{"code":"31","date":"Fri, 19 May 2017 10:00 PM EEST","temp":"22","text":"Clear"}}}}}}

## Prerequisites
- JDK 1.8 or later
- Maven 3 or later
- MYSQL 5.0 or later
- Schema with name 'orange' needs to be created.

## Stack
- Spring Security
- Spring Boot
- Spring Data JPA
- Maven
- JSP (view)
- HSQL

## Run
```mvn clean spring-boot:run```
OR
run the main class as java application from your IDE "AssignmentApplication.java"
