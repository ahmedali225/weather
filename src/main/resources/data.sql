--Adding User Roles
insert into role (name, description) values ("Admin", "Admin Role Has Access TO Management Pages");
insert into role (name, description) values ("Employee", "Emplyee Role Has Access Only To Weather Info");

--Adding predefined weather notes
insert into weather_notes (note_text, range_start, range_end, predefined) values ("It's very cold today, make sure you have a coat.", 1.0, 10.0, true);
insert into weather_notes (note_text, range_start, range_end, predefined) values ("It's a little bit cold today, make sure you have a sweater.", 10.0, 15.0, true);
insert into weather_notes (note_text, range_start, range_end, predefined) values ("The weather is nice today, have a good day.", 15.0, 20.0, true);
insert into weather_notes (note_text, range_start, range_end, predefined) values ("It's hot today.", 20.0, 100.0, true);

--Adding Admin User with password adminadmin
insert into user (active, email, mobile_number, name, password, username, role_id) values (true, "admin@weather.com", "01007525630", "Ahmed Ali", "$2a$10$aFBxTvzC978tpLmuE5sm6ebin5K2C1tdw2UghcEOPrAS7vYXwyHxS", "admin", 1);
