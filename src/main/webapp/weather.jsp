<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>Create an account</title>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<h2>
				Welcome ${pageContext.request.userPrincipal.name} | <a
					onclick="document.forms['logoutForm'].submit()">Logout</a>
			</h2>
		</c:if>

		<form:form method="POST" modelAttribute="serviceParams"
			class="form-signin">
			<h2 class="form-signin-heading">Find out the current weather
				conditions in your city:</h2>
			<spring:bind path="city">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<form:input type="text" path="city" class="form-control"
						placeholder="City" autofocus="true"></form:input>
					<form:errors path="city"></form:errors>
				</div>
			</spring:bind>

			<spring:bind path="country">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<form:input type="text" path="country" class="form-control"
						placeholder="Country"></form:input>
					<form:errors path="country"></form:errors>
				</div>
			</spring:bind>

			<button class="btn btn-lg btn-primary btn-block" type="submit">Find
				Out</button>
		<div class="row">
			<c:if test="${temp != null }">
				<span>Today's temprature is: ${temp}</span>
			</c:if>
			<c:if test="${text != null }">
				<span>, and overall condition is ${text}.</span>
			</c:if>
		</div>
		<div class="row">
			<c:if test="${adminNote != null }">
				<span>${adminNote}</span>
			</c:if>
		</div>
		</form:form>
	</div>
	<!-- /container -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
