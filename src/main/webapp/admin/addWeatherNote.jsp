<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create an account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

		
		<c:if test="${pageContext.request.userPrincipal.name != null}">
				<h2>Welcome ${pageContext.request.userPrincipal.name} | 
			<form id="notesForm" method="GET" action="${contextPath}/notesHistory">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<a onclick="document.forms['notesForm'].submit()">View Old Notes</a> |
			
			<form id="checkWeather" method="GET" action="${contextPath}/weather">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<a onclick="document.forms['checkWeather'].submit()">Check Weather</a> |
			
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<a onclick="document.forms['logoutForm'].submit()">Logout</a>
			</h2>
		</c:if>
		
		<form:form method="POST" modelAttribute="weatherNote" class="form-signin">
        <h2 class="form-signin-heading">Add Weather Note</h2>
        <spring:bind path="noteText">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="noteText" class="form-control" placeholder="Note Text"
                            autofocus="true"></form:input>
                <form:errors path="noteText"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="rangeStart">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="rangeStart" class="form-control" placeholder="Range Start"></form:input>
                <form:errors path="rangeStart"></form:errors>
            </div>
        </spring:bind>
        <spring:bind path="rangeEnd">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="rangeEnd" class="form-control" placeholder="Range End"></form:input>
                <form:errors path="rangeEnd"></form:errors>
            </div>
        </spring:bind>
		<span>${message}</span>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Add Note</button>
    </form:form>

</div>
<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
