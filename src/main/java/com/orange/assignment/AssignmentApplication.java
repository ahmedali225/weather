package com.orange.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class AssignmentApplication {
//	private static final Logger log = LoggerFactory.getLogger(AssignmentApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(AssignmentApplication.class, args);
//		RestTemplate restTemplate = new RestTemplate();
//		Weather weather = restTemplate.getForObject("https://query.yahooapis.com/v1/public/yql?q=select item.condition from weather.forecast where u='c' and woeid in (select woeid from geo.places(1) where text='Cairo, Egypt')&format=json", Weather.class);
//		log.info(weather.getQuery().getResults().getChannel().getItem().getCondition().toString()); 
	}
	
}
