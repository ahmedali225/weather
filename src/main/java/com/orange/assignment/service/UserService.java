package com.orange.assignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.orange.assignment.dao.RoleDao;
import com.orange.assignment.dao.UserDao;
import com.orange.assignment.model.User;

@Service
public class UserService {
	
	@Autowired
	protected UserDao userDao;
	
	@Autowired
	protected RoleDao roleDao;
	
	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setUserRole(roleDao.findByName("Employee"));
        userDao.save(user);
    }

//    public User findUserByCredentials(String username, String password) {
//        return userDao.findByUsernameAndPassword(username, password);
//    }
    
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

}
