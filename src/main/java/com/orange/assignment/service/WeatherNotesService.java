package com.orange.assignment.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.assignment.dao.UserDao;
import com.orange.assignment.dao.WeatherNotesDao;
import com.orange.assignment.model.User;
import com.orange.assignment.model.WeatherNotes;

@Service
public class WeatherNotesService {

	@Autowired
	private WeatherNotesDao weatherNotesDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private SecurityService securityService;
	
	public String getAdminNote(Double temprature) {
		LocalDate today = LocalDate.now();
		
		WeatherNotes weatherNote = weatherNotesDao.findByDateAddedAndPredefinedFalse(today);
		
		if (weatherNote != null) {
			return weatherNote.getNoteText();
		}
		
		weatherNote = findPredefinedNote(temprature);
		return weatherNote.getNoteText();
	}

	private WeatherNotes findPredefinedNote(Double temprature) {
		List<WeatherNotes> predefinedNotes = weatherNotesDao.findByPredefinedTrue();
		
		return predefinedNotes.stream().filter(note -> temprature > note.getRangeStart() && temprature < note.getRangeEnd()).findFirst().get();
	}

	public void addWeatherNote(WeatherNotes weatherNote) {
		User owner = userDao.findByUsername(securityService.findLoggedInUsername());
		weatherNote.setPredefined(false);
		weatherNote.setOwner(owner);
		weatherNote.setDateAdded(LocalDate.now());
        weatherNotesDao.save(weatherNote);
	}
}
