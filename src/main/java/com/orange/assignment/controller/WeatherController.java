package com.orange.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orange.assignment.service.WeatherNotesService;
import com.orange.assignment.validator.ServiceParamsValidator;
import com.orange.assignment.webservice.WeatherServiceHandler;
import com.orange.assignment.webservice.data.ServiceParams;
import com.orange.assignment.webservice.data.Weather;

@Controller
public class WeatherController {
	
	@Autowired
	private WeatherServiceHandler weatherServiceHandler;
	
	@Autowired
	private WeatherNotesService weatherNotesService;
	
	@Autowired
	private ServiceParamsValidator serviceValidator;

	@RequestMapping(value = "/weather", method = RequestMethod.GET)
    public String weather(Model model) {
        model.addAttribute("serviceParams", new ServiceParams());

        return "weather";
    }

    @RequestMapping(value = "/weather", method = RequestMethod.POST)
    public String weather(@ModelAttribute("serviceParams") ServiceParams serviceParams, BindingResult bindingResult, Model model) {
        serviceValidator.validate(serviceParams, bindingResult);

        if (bindingResult.hasErrors()) {
            return "weather";
        }
    	
    	Weather weather = weatherServiceHandler.findWeather(serviceParams.getCity(), serviceParams.getCountry());
    	String temp = weather.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
    	String text = weather.getQuery().getResults().getChannel().getItem().getCondition().getText();
    	
		String adminNote = weatherNotesService.getAdminNote(Double.valueOf(temp));

		model.addAttribute("temp", temp);
		model.addAttribute("text", text);
		model.addAttribute("adminNote", adminNote);
    	
        return "weather";
    }
    
}
