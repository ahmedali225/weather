package com.orange.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orange.assignment.dao.UserDao;
import com.orange.assignment.dao.WeatherNotesDao;
import com.orange.assignment.model.User;
import com.orange.assignment.model.WeatherNotes;
import com.orange.assignment.service.SecurityService;
import com.orange.assignment.service.WeatherNotesService;
import com.orange.assignment.validator.WeatherValidator;

@Controller
public class AdminController {
	
	@Autowired
	private WeatherNotesDao weatherNotesDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private SecurityService securityService;
	
	@Autowired
	private WeatherNotesService weatherService; 
	
	@Autowired
	private WeatherValidator weatherValidator;
	
	@RequestMapping(value = {"/admin", "/notesHistory"}, method = RequestMethod.GET)
    public String listNotesHistory(Model model) {
		User user = userDao.findByUsername(securityService.findLoggedInUsername());
		List<WeatherNotes> weatherNotes = weatherNotesDao.findByOwnerAndPredefinedFalseOrderByDateAddedDesc(user);
        model.addAttribute("notesList", weatherNotes);

        return "/admin/notesHistory";
    }
	
	@RequestMapping(value = {"/admin", "/addWeatherNote"}, method = RequestMethod.GET)
    public String addWeatherNote(Model model) {
        model.addAttribute("weatherNote", new WeatherNotes());

        return "/admin/addWeatherNote";
    }

	@RequestMapping(value = {"/admin", "/addWeatherNote"}, method = RequestMethod.POST)
    public String addWeatherNote(@ModelAttribute("weatherNote") WeatherNotes weatherNote, BindingResult bindingResult, Model model) {
        weatherValidator.validate(weatherNote, bindingResult);

        if (bindingResult.hasErrors()) {
            return "/admin/addWeatherNote";
        }
        
		weatherService.addWeatherNote(weatherNote);

		model.addAttribute("message", "Weather note was added successfully.");
		
        return "/admin/addWeatherNote";
    }

}
