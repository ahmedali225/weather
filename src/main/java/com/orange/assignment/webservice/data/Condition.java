package com.orange.assignment.webservice.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Condition {

	private String temp;
	
	private String text;

	
	public Condition() {
	}

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Condition [temp=" + temp + ", text=" + text + "]";
	}
	
}
