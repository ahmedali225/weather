package com.orange.assignment.webservice.data;

public class ServiceParams {
	
	private String city;

	private String country;

	public ServiceParams() {
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCouuntry(String couuntry) {
		this.country = couuntry;
	}

}
