package com.orange.assignment.webservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.orange.assignment.AssignmentApplication;
import com.orange.assignment.webservice.data.Weather;

@Service
public class WeatherServiceHandler {

	private static final Logger log = LoggerFactory.getLogger(AssignmentApplication.class);
	
	@Cacheable("weather")
	public Weather findWeather(String city, String country) {
		RestTemplate restTemplate = new RestTemplate();
		String serviceURL = "https://query.yahooapis.com/v1/public/yql?q=";
		String query = "select item.condition from weather.forecast where u='c' and "
				+ "woeid in (select woeid from geo.places(1) where text='" + city + ", " + country + "')&format=json";
		Weather weather = restTemplate.getForObject(serviceURL + query, Weather.class);
		log.info(weather.getQuery().getResults().getChannel().getItem().getCondition().toString()); 
		return weather;
	}
	
}
