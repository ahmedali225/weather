package com.orange.assignment.dao;

import org.springframework.data.repository.CrudRepository;

import com.orange.assignment.model.Role;

public interface RoleDao extends CrudRepository<Role, Long> {

	Role findByName(String string);

}
