package com.orange.assignment.dao;

import org.springframework.data.repository.CrudRepository;

import com.orange.assignment.model.User;

public interface UserDao extends CrudRepository<User, Long> {
	
	User findByEmail(String email);

	User findByUsername(String userName);

}
