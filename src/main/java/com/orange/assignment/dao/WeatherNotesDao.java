package com.orange.assignment.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.orange.assignment.model.User;
import com.orange.assignment.model.WeatherNotes;

public interface WeatherNotesDao extends CrudRepository<WeatherNotes, Long> {

	WeatherNotes findByDateAddedAndPredefinedFalse(LocalDate today);
	
	List<WeatherNotes> findByOwnerOrderByDateAddedDesc(User owner);
	
	List<WeatherNotes> findByPredefinedTrue();

	List<WeatherNotes> findByOwnerAndPredefinedFalseOrderByDateAddedDesc(User user);
}
