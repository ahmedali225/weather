package com.orange.assignment.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.orange.assignment.webservice.data.ServiceParams;

@Component
public class ServiceParamsValidator implements Validator{

	@Override
	public boolean supports(Class<?> aClass) {
		return ServiceParams.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
//		ServiceParams serviceParams = (ServiceParams) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "CityName");
	}

}
