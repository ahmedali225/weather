package com.orange.assignment.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.orange.assignment.model.WeatherNotes;

@Component
public class WeatherValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return WeatherNotes.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
    	WeatherNotes weatherNotes = (WeatherNotes) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "noteText", "NotEmpty");
        if (weatherNotes.getNoteText().length() > 256) {
            errors.rejectValue("noteText", "Size");
        }
    }
}
