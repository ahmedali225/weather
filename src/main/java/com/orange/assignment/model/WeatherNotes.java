package com.orange.assignment.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="weather_notes")
public class WeatherNotes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String noteText;
	
	private Double rangeStart;
	
	private Double rangeEnd;
	
	private LocalDate dateAdded;
	
	private Boolean predefined;
	
	@ManyToOne(optional=true)
	@JoinColumn(name = "user_id")
	private User owner;
}
