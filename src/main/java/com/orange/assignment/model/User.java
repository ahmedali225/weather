package com.orange.assignment.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@Column(unique=true)
	private String username;
	
	private String name;
	
	@Email
	private String email;
	
	private String password;
	
	private String mobileNumber;
	
	private Boolean active;
	
	@OneToMany(cascade=CascadeType.REMOVE, mappedBy="owner")
	private List<WeatherNotes> notes;
	
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role userRole;
	
}
